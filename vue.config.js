module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    proxy: 'http://openapi.seoul.go.kr:8088',
    disableHostCheck: true
  }
}
