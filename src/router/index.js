import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		component: () => import('@/views/Home.vue'),
	},
  {
		path: '/d3Tree',
    name: 'd3Tree',
		component: () => import('@/views/D3Tree.vue'),
	},
  {
		path: '/d3Line',
    name: 'd3Line',
		component: () => import('@/views/D3Line.vue'),
	},
	// {
	// 	path: '*',
	// 	name: 'error',
	// 	component: () => import('../views/error'),
	// },
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
