import Vue from 'vue'
import App from './App.vue'
import router from './router';
import vuetify from './plugins/vuetify'
import chart from 'vue-chartjs'
import * as d3 from "d3";
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  chart,
  d3,
  render: h => h(App)
}).$mount('#app')
